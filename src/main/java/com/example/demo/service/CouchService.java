package com.example.demo.service;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.json.JsonObject;
import com.couchbase.client.java.kv.GetResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CouchService {

  @Autowired
  private Cluster cluster;

  public void addDocumentToTestBucket() {
    Bucket bucket = cluster.bucket("test_bucket");
    Collection collection = bucket.defaultCollection();
    collection.upsert("document-1", JsonObject
        .create().put("name", "Mike"));
  }

  public void fetchResultFromTestBucket() {
    Bucket bucket = cluster.bucket("test_bucket");
    Collection collection = bucket.defaultCollection();
    GetResult result = collection.get("document-1");
    System.out.println(result.contentAsObject());
  }

  public void fetchResultFromMars() {
    Bucket bucket = cluster.bucket("mars_core_local_dev");
    Collection collection = bucket.defaultCollection();
    GetResult result = collection.get("key::011040_FW2019");
    System.out.println(result.contentAsObject());
  }

}
