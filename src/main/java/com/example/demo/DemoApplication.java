package com.example.demo;

import com.couchbase.client.java.Cluster;
import com.example.demo.service.CouchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {

  public static void main(String[] args) {
    SpringApplication.run(DemoApplication.class, args);
  }


  @Autowired
  private CouchService couchservice;

  @Bean
  CommandLineRunner run1(final Cluster cluster) {

    return strings -> {
//      couchservice.addDocumentToTestBucket(cluster);
//      couchservice.fetchResultFromMars(cluster);
    };
  }
}
