package com.example.demo.conf;

import com.couchbase.client.java.Cluster;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CouchbaseConfig {
  @Bean
  public Cluster getCluster() {
    return Cluster.connect("localhost", "Administrator", "123456");
  }
}
