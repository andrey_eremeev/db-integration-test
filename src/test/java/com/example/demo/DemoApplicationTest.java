package com.example.demo;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.kv.GetResult;
import com.example.demo.service.CouchService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = AbstractSharedTestcontainerTest.class)
class DemoApplicationTest extends AbstractSharedTestcontainerTest {

  @Autowired
  CouchService couchService;

  @Autowired
  Cluster cluster;

  @Test
  void addDocumentToTestBucket() {
    couchService.addDocumentToTestBucket();
    Bucket testBucket = cluster.bucket(BUCKET_NAME);
    Collection collection = testBucket.defaultCollection();
    GetResult documentResult = collection.get("document-1");
    Assert.assertNotNull(documentResult);
    Assert.assertEquals(documentResult.contentAsObject().get("name"), "Mike");
  }

  @Test
  void testMetadataInit() {
    Bucket testBucket = cluster.bucket(BUCKET_NAME);
    Collection collection = testBucket.defaultCollection();
    GetResult documentResult = collection.get("key::803");
    Assert.assertNotNull(documentResult);
    Assert.assertEquals(documentResult.contentAsObject().get("code"), "Reebok");
  }
}