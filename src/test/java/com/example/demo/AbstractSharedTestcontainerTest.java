package com.example.demo;

import com.couchbase.client.core.env.SeedNode;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.ClusterOptions;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.FixedHostPortGenericContainer;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.HttpWaitStrategy;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Testcontainers
@TestConfiguration
abstract public class AbstractSharedTestcontainerTest {

  public static final String CLUSTER_USERNAME = "Administrator";
  public static final String CLUSTER_PASSWORD = "123456";
  public static final String BUCKET_NAME = "test_bucket";

//  "static" indicates that container will be shared between all tests inherited from this class.
//  if you need container that will be restarted after each test method use instance variable
  @Container
  public static GenericContainer couchContainer = new FixedHostPortGenericContainer<>("couchbase")
//      couchbase is very sensitive to change of ports. So far I couldn't deal with random ports exposed from container.
//      list of ports necessary for docker to work (actually there are much more ports https://docs.couchbase.com/server/6.5/install/install-ports.html)
      .withFixedExposedPort(8091, 8091)
      .withFixedExposedPort(8092, 8092)
      .withFixedExposedPort(8093, 8093)
      .withFixedExposedPort(8094, 8094)
      .withFixedExposedPort(11210, 11210)
      .withClasspathResourceMapping("RMA_Structure_short.json",
          "/etc/RMA_Structure_short.json", BindMode.READ_ONLY)
//      waiting for availability management port 8091
    .waitingFor(new HttpWaitStrategy().forPort(8091));

  @BeforeAll
  static void setUp() throws IOException, InterruptedException {
    couchContainer.execInContainer(
        "couchbase-cli",
        "cluster-init",
        "-c=127.0.0.1",
        "--cluster-username=" + CLUSTER_USERNAME,
        "--cluster-password=" + CLUSTER_PASSWORD,
        "--services=data",
        "--cluster-ramsize=256");
    couchContainer.execInContainer("couchbase-cli",
        "bucket-create",
        "-c=localhost:8091",
        "--username=" + CLUSTER_USERNAME,
        "--password=" + CLUSTER_PASSWORD,
        "--bucket=" + BUCKET_NAME,
        "--bucket-type=couchbase",
        "--bucket-ramsize=128");
    couchContainer.execInContainer("cbimport", "json",
        "-c", "couchbase://127.0.0.1",
        "-u", CLUSTER_USERNAME,
        "-p", CLUSTER_PASSWORD,
        "-b" , BUCKET_NAME,
        "-d", "file:///etc/RMA_Structure_short.json",
        "-f", "list",
        "-g", "key::%id%",
        "-t", "4");
  }

  @TestConfiguration
  static class ClusterConfig {
    @Bean
    @Primary
    public Cluster getClusterTest() {
      Set<SeedNode> seedNodes = new HashSet<>(Arrays.asList(
          SeedNode.create(couchContainer.getContainerIpAddress(),
              Optional.empty(),
              Optional.empty())));
      return Cluster.connect(seedNodes, ClusterOptions.clusterOptions(CLUSTER_USERNAME, CLUSTER_PASSWORD));
    }
  }
}
